#!/bin/bash
set -euo pipefail

# Intercept the Ctrl-C signal to be able to stop the call to buildah
trap ctrl_c INT

function say { echo "$@" | toilet -f mono12 -w 400 | lolcat -f || true; }
function echo_green { echo -e "\e[1;32m${1}\e[0m"; }
function echo_red { echo -e "\e[1;31m${1}\e[0m"; }
function echo_yellow { echo -e "\e[1;33m${1}\e[0m"; }

declare -i FAILED=0
declare child_pid

function ctrl_c {
    kill -9 "$child_pid"
    echo_red "Operation terminated by the user"
    exit 1
}

function loop {
    _loop 5 10 only_notify "$@"
}

function loop_or_fail {
    _loop 5 10 exit_on_fail "$@"
}

function _loop {
    local count=$1 delay=$2 exit_on_fail=$3 s
    shift 3
    for i in $(seq "$count"); do
        # we want set -e still in effect, but also get the exit code; so
        # use a background process, as set -e is kept for background
        # commands, and the exit code checking is possible with wait later
        "$@" &
        child_pid=$!
        wait $child_pid && s=0 && break || s=$? && sleep "$delay"
    done
    FAILED+=$((s > 0))
    if [ "$exit_on_fail" = "exit_on_fail" ]; then
        (exit $s)
    fi
}

loop $*
