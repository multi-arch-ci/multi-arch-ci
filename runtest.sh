#!/bin/bash

hwpf=$(uname -i)
MCI_DIR=$(dirname "${BASH_SOURCE[0]}")
KVM_ARCH=""
if (egrep -q 'vmx' /proc/cpuinfo); then
    KVM_ARCH="kvm_intel"
elif (egrep -q 'svm' /proc/cpuinfo); then
    KVM_ARCH="kvm_amd"
elif [[ $hwpf == "ppc64" || $hwpf == "ppc64le" ]]; then
    KVM_ARCH="kvm_hv"
fi

# Reload the modules
modprobe -a -f kvm $KVM_ARCH

# Fix the kvm permissions
chmod 0666 /dev/kvm ; sleep 3
mknod -m 0660 /dev/loop0 b 7 0
losetup -f
losetup -d /dev/loop0
systemctl start --now libvirtd

# Run the tests
python3 ${MCI_DIR}/daily.py $*
