#!/bin/bash

PIPELINE_ID=$1
TOKEN=$2
POJECT_URLS=("https://gitlab.com/api/v4/projects/29231673/pipelines?scope=running" "https://gitlab.com/api/v4/projects/29231673/pipelines?scope=pending" "https://gitlab.com/api/v4/projects/29231072/pipelines?scope=running" "https://gitlab.com/api/v4/projects/29231072/pipelines?scope=pending")
while true; do
	pipelines=()
	for url in ${POJECT_URLS[@]}; do
		api_response=$(curl -fs -H "PRIVATE-TOKEN: $TOKEN" $url)
		curl_exit_code=$?
		if [[ $curl_exit_code -ne 0 ]]; then
			exit $curl_exit_code;
		fi
		pipelines+=$(echo $api_response | jq '.[] | .id')
	done
	is_runner_used=false
	for pipeline_id in $pipelines; do
		if [[ pipeline_id -ne $PIPELINE_ID ]]; then
			echo "Some pipeline is still runnig. Waiting for next 60 seconds"
			is_runner_used=true
			break
		fi
	done
	if $is_runner_used; then
		sleep 60
	else
		break
	fi
done
echo "all runners are idle"
