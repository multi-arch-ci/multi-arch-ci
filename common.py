#!/usr/bin/env python3

import os

from avocado.core.suite import TestSuite
from avocado.core.settings import settings
from virttest import utils_misc


def get_proc_model():
    arch = os.uname().machine

    info = utils_misc.get_cpu_info()

    if arch == 's390x':
        zfamily_map = {'2964': 'z13',
                       '3906': 'z14',
                       '8561': 'z15'
                       }
        model = zfamily_map[info['Machine type']].lower()
    elif 'Model name' in info.keys():
        model = info['Model name']
    else:
        model = 'Unknown'
    return model


class CommonSuiteCreator:

    def __init__(self, arch, config, guest_os_variant, host_os_variant):
        self.arch = arch
        self._config = settings.as_dict()
        self._config.update(config)
        self.guest_os_variant = guest_os_variant
        self.host_os_variant = host_os_variant

    @property
    def config(self):
        return self._config.copy()

    @property
    def extra_params(self):
        rhel_url = self.get_install_url()
        return ['automem=no', 'qemu_sandbox= \'\'', 'url=%s' % rhel_url]

    def get_install_url(self):
        if "RHEL-8" in self.guest_os_variant:
            return ("http://download.devel.redhat.com/rhel-8/nightly/RHEL-8/"
                    "latest-%s/compose/BaseOS/%s/os/") % (self.guest_os_variant,
                                                          self.arch)
        elif "RHEL-9" in self.guest_os_variant:
            return ("http://download.devel.redhat.com/rhel-9/nightly/RHEL-9/"
                    "latest-%s/compose/BaseOS/%s/os/") % (self.guest_os_variant,
                                                          self.arch)
        else:
            return ("http://download.devel.redhat.com/composes/finished/latest-%s/"
                    "compose/Server/%s/os/") % (self.guest_os_variant,
                                                self.arch)

    def functional_test(self, variant, references, extra_params=[],
                        vt_type='qemu', hugepages=False):
        suites = []
        rhel_url = self.get_install_url()
        if variant is not None:
            variant_name = '%s-%s' % (self.arch, variant)
        else:
            variant_name = '%s' % (self.arch)

        config = self.config
        config['resolver.references'] = references
        config['vt.type'] = vt_type

        extra = self.extra_params
        extra.append('image_size=15G')
        extra.append('backup_image_after_testing_passed=no')
        if "RHEL-9" in self.guest_os_variant:
            extra.append('guest_dmesg_level=2')
        if ("RHEL" in self.guest_os_variant) and ("url" in variant_name):
            extra.append('kickstart_extra_repos=%s' % rhel_url.replace("BaseOS", "AppStream"))
        config['vt.extra_params'] = extra + extra_params

        if hugepages:
            config['vt.filter.default_filters'] = "smallpages"
            config['vt.only_filter'] = "hugepages"

        suites.append(TestSuite.from_config(name=variant_name, config=config))
        return suites

    def migration_test(self):
        if "ppc64" in self.arch:
            print(f"{self.arch} is not supported for migration tests")
            machine_type = "pseries-rhel8.6.0"
            return []
        if ("RHEL-8" in self.host_os_variant) and ("s390x" in self.arch):
            machine_type = "s390-ccw-virtio-rhel8.6.0"
        if ("RHEL-8" in self.host_os_variant) and ("aarch64" in self.arch):
            machine_type = "arm64-pci:virt-rhel8.6.0"
        if ("RHEL-8" in self.host_os_variant) and ("x86_64" in self.arch):
            machine_type = "pc-q35-rhel8.6.0"
        if ("RHEL-9" in self.host_os_variant) and ("s390x" in self.arch):
            machine_type = "s390-ccw-virtio-rhel9.0.0"
        if ("RHEL-9" in self.host_os_variant) and ("aarch64" in self.arch):
            machine_type = "arm64-pci:virt-rhel9.0.0"
        if ("RHEL-9" in self.host_os_variant) and ("x86_64" in self.arch):
            machine_type = "pc-q35-rhel9.0.0"

        config = self.config
        config['vt.qemu.qemu_dst_bin'] = "/root/qemu-bin/bin/qemu-kvm"
        extra = []
        extra.append('image_size=15G')
        extra.append('backup_image_after_testing_passed=no')
        extra.append(f'machine_type={machine_type}')
        if "RHEL-9" in self.guest_os_variant:
            extra.append('guest_dmesg_level=2')
        config['vt.extra_params'] = extra

        if "RHEL-8" in self.host_os_variant:
            test_name="migration-rhel8-master"
        elif "RHEL-9" in self.host_os_variant:
            test_name="migration-rhel9-master"

        config['resolver.references'] = ["unattended_install.cdrom.extra_cdrom_ks.default_install.aio_threads", "migrate.default.tcp"]
        suites = [TestSuite.from_config(name=test_name, config=config)]

        return suites
