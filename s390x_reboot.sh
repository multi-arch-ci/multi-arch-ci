#!/bin/bash

OS_VERSION=$1
SSH_HOST=$2

if [ -z "${OS_VERSION}" ]; then
  echo "ERROR OS_VERSION not set."
  exit -1
fi
if [ -z "${SSH_HOST}" ]; then
  echo "ERROR SSH_HOST not set."
  exit -1
fi

if [ "${OS_VERSION}" == "rhel8" ]; then
  CMD="chreipl fcp -d 0.0.a400 -w 0x5005076810254e60 -l 0x0000000000000000"
elif [ "${OS_VERSION}" == "rhel9" ]; then
  CMD="chreipl fcp -d 0.0.a400 -w 0x5005076810254e60 -l 0x1000000000000"
fi

ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null root@${SSH_HOST} "
  eval ${CMD}
  reboot
  exit
"

sleep 5

until ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=2 "${SSH_HOST}" exit
  do sleep 1
done
