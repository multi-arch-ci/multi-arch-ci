import sys
import argparse
import distro
import os

from common import CommonSuiteCreator, get_proc_model
from avocado.core.job import Job

parser = argparse.ArgumentParser(description='Run Multi-Arch-CI tests for ARM')
parser.add_argument('-g', '--guest-os', default='8', help='Specify the Guest OS Version to use', dest='guest')
parser.add_argument('-r', '--runner', default='nrunner', help='Specify which Avocado Runner to use', dest='runner')
parser.add_argument('-l', '--lite', action='store_true', help='Run only first test_suite', dest='lite')
parser.add_argument('-m', '--migration', action='store_true', help='Run only migration test_suite', dest='migration')
parser.add_argument('--libvirt-dvd', action='store_true', help='Run only libvirt/dvd test_suite', dest='libvirt_dvd')
parser.add_argument('--libvirt-url', action='store_true', help='Run only libvirt/url test_suite', dest='libvirt_url')
parser.add_argument('--qemu-dvd', action='store_true', help='Run only qemu/dvd test_suite', dest='qemu_dvd')
parser.add_argument('--qemu-url', action='store_true', help='Run only qemu/url test_suite', dest='qemu_url')
parser.add_argument('--hugepages', action='store_true', help='Run only hugepages test_suite', dest='hugepages')
parser.add_argument('-n', '--new-qemu', action='store_true', help='Use the Rebased compiled QEMU version', dest='new_qemu')

args = parser.parse_args()

ARCH = os.uname().machine
HOST_OS = "%s-%s" % (distro.id().upper(), distro.version())
ISO_DIR = "/var/lib/libvirt/images/avocado/avocado-vt/isos/linux"
GUEST_OS = "RHEL.8.devel"
if args.guest == '9':
    GUEST_OS = "RHEL.9.devel"
ISO = "%s-%s.iso" % (GUEST_OS.replace('.', '-'), ARCH)
ISO_FULL_PATH = "%s/%s" % (ISO_DIR, ISO)
GUEST_OS_VARIANT = os.path.basename(os.readlink(ISO_FULL_PATH))[0:10]
QEMU_BIN = "/usr/libexec/qemu-kvm"
if args.new_qemu:
    QEMU_BIN = "/root/qemu-bin/bin/qemu-kvm"

print("[INFO] Host OS: %s-%s" % (distro.id().upper(), distro.version()))
print("[INFO] Host Kernel: %s" % os.uname().release)
print("[INFO] Host ARCH: %s" % ARCH)
print("[INFO] Host Processor: %s" % get_proc_model())
print("[INFO] Guest OS: %s" % GUEST_OS_VARIANT)
print("[INFO] Guest OS ISO: %s" % ISO_FULL_PATH)
print("[INFO] Avocado runner: %s" % args.runner)
if not os.path.isfile(ISO_FULL_PATH):
    print("[ERROR] Guest OS image does not exist, consired running avocado_get_rhel_dvd")
    sys.exit(-1)

arguments = ["lite", "migration", "libvirt_dvd", "libvirt_url", "qemu_dvd",
             "qemu_url", "hugepages"]
if not any(vars(args)[argument] for argument in arguments):
    args.libvirt_dvd = True
    args.libvirt_url = True
    args.qemu_dvd = True
    args.qemu_url = True
    args.hugepages = True

config = {'core.show': ['app'],
          'run.test_runner': args.runner,
          'run.failfast': True,
          'nrunner.max_parallel_tasks': 1,
          'vt.guest_os': GUEST_OS,
          'vt.common.arch': ARCH,
          'vt.qemu.qemu_bin': QEMU_BIN,
          'vt.qemu.qemu_dst_bin': QEMU_BIN}

creator = CommonSuiteCreator(arch=ARCH,
                             config=config,
                             guest_os_variant=GUEST_OS_VARIANT,
                             host_os_variant=HOST_OS)
suites = []

install_dvd = "unattended_install.cdrom.extra_cdrom_ks.default_install.aio_threads"
install_url = "unattended_install.url.extra_cdrom_ks.default_install.aio_threads"

test_references = ["io-github-autotest-qemu.boot", "remove_guest.without_disk"]
if args.libvirt_dvd or args.lite:
    suites = suites + creator.functional_test("libvirt/dvd",
                                              [install_dvd] + test_references,
                                              vt_type="libvirt")

test_references = ["io-github-autotest-qemu.boot", "remove_guest.without_disk"]
if args.libvirt_url:
    suites = suites + creator.functional_test("libvirt/url",
                                              [install_url] + test_references,
                                              vt_type="libvirt")

test_references = ["io-github-autotest-qemu.boot", "migrate.with_reboot.tcp",
                   "migrate.default.tcp..with_post_copy",
                   "migrate.default.tcp.with_filter_off.with_multifd"]
vt_extra_params = ["ping_pong=5"]
if args.qemu_dvd:
    suites = suites + creator.functional_test("%s/dvd" % GUEST_OS_VARIANT,
                                              [install_dvd] + test_references,
                                              vt_extra_params)

if args.qemu_url:
    suites = suites + creator.functional_test("%s/url" % GUEST_OS_VARIANT,
                                              [install_url] + test_references,
                                              vt_extra_params)

vt_extra_params = ["ping_pong=5", "mig_speed=1G", "pre_migrate=mig_set_speed"]
if args.hugepages:
    suites = suites + creator.functional_test("%s/dvd/HP" % GUEST_OS_VARIANT,
                                              [install_dvd] + test_references,
                                              vt_extra_params,
                                              hugepages=True)
if args.migration:
    suites = suites + creator.migration_test()

with Job(config, suites) as j:
    j.run()
    sys.exit(j.result.tests_total is not j.result.passed)
